#include <windows.h>
#include <iostream>

typedef int (__stdcall *f_simpleAdd)(std::string device);

int main(int argc, char **argv) {
	if (argc != 2) {
		std::cerr << "[ERROR] Please give openCL.dll as argument input..." << std::endl;
		return (EXIT_FAILURE);
	}

	HINSTANCE hGetProcIDDLL = LoadLibrary(argv[1]);

	if (!hGetProcIDDLL) {
		std::cerr << "[ERROR] Could not load the dynamic library..." << std::endl;
		return (EXIT_FAILURE);
	}

	f_simpleAdd simpleAdd = (f_simpleAdd)GetProcAddress(hGetProcIDDLL, "simpleAdd");
	if (!simpleAdd) {
		std::cerr << "[ERROR] Could not locate the function..." << std::endl;
		return (EXIT_FAILURE);
	}

	simpleAdd("CPU");
	simpleAdd("GPU");

	return (EXIT_SUCCESS);
}
