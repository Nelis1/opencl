#ifndef OPENCLDll_HPP
# define OPENCLDll_HPP

# include <iostream>
# include <vector>
# include <memory>
# include <algorithm>

#include <string>
#include <fstream>
#include <streambuf>

const size_t batchSize = 10;

# define OPENCL_API __declspec(dllexport)

# ifdef __cplusplus
	extern "C" {
# endif

OPENCL_API int simpleAdd(std::string device);

# ifdef __cplusplus
	}
# endif
#endif
