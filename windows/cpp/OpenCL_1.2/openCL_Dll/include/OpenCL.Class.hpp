#ifndef OPENCL_CLASS_HPP
# define OPENCL_CLASS_HPP

# define CL_HPP_MINIMUM_OPENCL_VERSION 120
# define CL_HPP_TARGET_OPENCL_VERSION 120
# define CL_TARGET_OPENCL_VERSION 120

# include <CL/cl2.hpp>
# include "openCLDll.hpp"

class OpenCL {
	public:
		// Constructors:
		OpenCL(std::string device);
		~OpenCL(void);

		// Functions:
		void OpenCL::createBuffers(void);
		void OpenCL::createQueue(void);
		void OpenCL::pushDataToDevice(const float A[], const float B[]);
		void OpenCL::setArguments(void);
		void OpenCL::setQueues(void);
		void OpenCL::readBuffer(float (*C)[batchSize]);

	private:
		// Variables:
		std::vector<cl::Platform>	all_platforms;
		cl::Platform				default_platform;
		cl::Device					default_device;
		cl::Context					context;
		cl::Program					program;
		cl::CommandQueue			queue;

		// Memory objects:
		cl::Buffer					buffer_A;
		cl::Buffer					buffer_B;
		cl::Buffer					buffer_C;

		// Kernel functions:
		cl::Kernel					simple_add;
};

#endif
