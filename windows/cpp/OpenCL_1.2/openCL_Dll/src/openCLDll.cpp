#include "OpenCL.Class.hpp"

OPENCL_API int simpleAdd(std::string device) {
	OpenCL openCL(device);
	openCL.createBuffers();	// Create buffers on the device.

	float A[batchSize];
	float B[batchSize];

	// Generate some random numbers and store in A & B:
	srand (1);
	for (int j = 0; j < batchSize; j++) {
		float i = rand() % 101;
		A[j] = i;
		i = rand() % 101;
		B[j] = i;
	}

	openCL.createQueue();			// Create queue to which we will push commands for the device.
	openCL.pushDataToDevice(A, B);	// Write arrays A and B to the device.
	openCL.setArguments();			// Set input arguments for function to be called.
	openCL.setQueues();

	float C[batchSize];
	openCL.readBuffer(&C);

	// Print return values from the c array:
	std::cout << "Result:" << std::endl;
	for (int i = 0; i < batchSize; i++) {
		std::cout << "\t" << i << ":\t" << C[i] << std::endl;
	}
	return (EXIT_SUCCESS);
};
