#include "OpenCL.Class.hpp"

OpenCL::OpenCL(std::string device) {
	std::cout << "Default Constructor Called" << std::endl;
	cl::Platform::get(&this->all_platforms);
	if (this->all_platforms.size() == 0) {
		std::cerr << "No platforms found. Check OpenCL installation!" << std::endl;
		exit(1);
	}

	this->default_platform = this->all_platforms[0];
	std::cout << "Using platform: " << this->default_platform.getInfo<CL_PLATFORM_NAME>() << std::endl;

	// Assign desired platform as default device:
	std::vector<cl::Device> all_devices;
	if (device == "CPU")
		this->default_platform.getDevices(CL_DEVICE_TYPE_CPU, &all_devices);
	else if (device == "GPU")
		this->default_platform.getDevices(CL_DEVICE_TYPE_GPU, &all_devices);
	else {
		std::cerr << "Invalid device type selected. Options: CPU/GPU" << std::endl;
		exit(1);
	}

	if (all_devices.size() == 0){
		std::cerr << "No devices found. Check OpenCL installation!" << std::endl;
		exit(1);
	}
	this->default_device = all_devices[0];
	std::cout << "Using device: " << this->default_device.getInfo<CL_DEVICE_NAME>() << std::endl;

	this->context = cl::Context({this->default_device}); // Get context for desired device.

	cl::Program::Sources sources;

	// Read code from file to string:
	std::ifstream codeFile("code.c");
	std::string kernel_code((std::istreambuf_iterator<char>(codeFile)), std::istreambuf_iterator<char>());

	// Store string code in cl_program_source_code variable:
	sources.push_back( {
		kernel_code.c_str(),
		kernel_code.length()
	});

	// Set program with acceleration device and the string source code:
	this->program = cl::Program(this->context, sources);

	// Compile the string code on acceleration device:
	if (this->program.build({this->default_device}) != CL_SUCCESS){
		std::cerr << "Error building: " << this->program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << std::endl;
		exit(1);
	}
	return ;
}

OpenCL::~OpenCL(void) {
	std::cout << "Destructor called." << std::endl;
	return ;
}

// Create buffers on the device:
void OpenCL::createBuffers(void) {
	this->buffer_A = cl::Buffer(this->context, CL_MEM_READ_WRITE, sizeof(float) * batchSize);
	this->buffer_B = cl::Buffer(this->context, CL_MEM_READ_WRITE, sizeof(float) * batchSize);
	this->buffer_C = cl::Buffer(this->context, CL_MEM_READ_WRITE, sizeof(float) * batchSize);
}

// Create queue to which we will push commands for the device:
void OpenCL::createQueue(void) {
	this->queue = cl::CommandQueue(this->context, this->default_device);
}

// Write arrays A and B to the device:
void OpenCL::pushDataToDevice(const float A[], const float B[]) {
	this->queue.enqueueWriteBuffer(this->buffer_A, CL_TRUE, 0, sizeof(float) * batchSize, A);
	this->queue.enqueueWriteBuffer(this->buffer_B, CL_TRUE, 0, sizeof(float) * batchSize, B);
}

void OpenCL::setArguments(void) {
	this->simple_add = cl::Kernel(this->program, "simple_add");	// Find function by name on acceleration device.

	// Set input arguments for function to be called:
	this->simple_add.setArg(0, this->buffer_A);
	this->simple_add.setArg(1, this->buffer_B);
	this->simple_add.setArg(2, this->buffer_C);
}

void OpenCL::setQueues(void) {
	this->queue.enqueueNDRangeKernel(this->simple_add, cl::NullRange, cl::NDRange(batchSize), cl::NullRange);	// Add to queue to be processed.
	// Can add more queues here before calling finish.
	this->queue.finish();
}

void OpenCL::readBuffer(float (*C)[batchSize]) {
	this->queue.enqueueReadBuffer(this->buffer_C, CL_TRUE, 0, sizeof(float) * batchSize, *C);	// Read result to C from the device to array C.
}
