#include <stdio.h>
#include <OpenCL/opencl.h>
#define DATA_SIZE (1024)

const char *KernelSource =
	"__kernel void	square(__global float *input, __global float *output, const unsigned int count)	\n" \
	"{																								\n" \
	"	int	i = get_global_id(0);																	\n" \
	"	if (i < count)																				\n" \
	"		output[i] = input[i] * input[i];														\n" \
	"}																								\n";

int	main(void)
{
	int					err = 0;				// error code returned from api calls

	float				data[DATA_SIZE];		// original data set given to device
	float				results[DATA_SIZE];		// results returned from device
	unsigned int		correct;				// number of correct results returned

	size_t				global;					// global domain size for our calculation
	size_t				local;					// local domain size for our calculation

	cl_device_id		device_id;				// compute device id 
	cl_context			context;				// compute context
	cl_command_queue	commands;				// compute command queue
	cl_program			program;				// compute program
	cl_kernel			kernel;					// compute kernel

	cl_mem				input;					// device memory used for the input array
	cl_mem				output;					// device memory used for the output array

	// ------------------------------ Fill our data set with zero float values ---------------------------
	int				i = 0;
	unsigned int	count = DATA_SIZE;

	for (i = 0; i < count; i++)
		data[i] = 0.0f;
	// ---------------------------------------------------------------------------------------------------

	// ------------------------------------- Connect to a compute device ---------------------------------
	err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);				// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clGetDeviceIDs.html
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		return (EXIT_FAILURE);
	}
	char	buffer[1024];
	clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(buffer), buffer, NULL);
	printf("CL_DEVICE_NAME: %s\n", buffer);
	// ---------------------------------------------------------------------------------------------------

	// --------------------------------------- Create a compute context ----------------------------------
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);												// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateContext.html
	if (!context)
	{
		printf("Error: Failed to create a compute context!\n");
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// --------------------------------------- Create a command commands ---------------------------------
	commands = clCreateCommandQueue(context, device_id, 0, &err);												// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateCommandQueue.html
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// --------------------------- Create the compute program from the source buffer ---------------------
	program = clCreateProgramWithSource(context, 1, (const char **) &KernelSource, NULL, &err);					// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateProgramWithSource.html
	if (!program)
	{
		printf("Error: Failed to create compute program!\n");
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// --------------------------------------- Build the program executable ------------------------------
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);													// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clBuildProgram.html
	if (err != CL_SUCCESS)
	{
		size_t	len;
		char	buffer[2048];

		printf("Error: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);			// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clGetProgramBuildInfo.html
		printf("%s\n", buffer);
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ------------------------ Create the compute kernel in the program we wish to run ------------------
	kernel = clCreateKernel(program, "square", &err);															// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateKernel.html
	if ((!kernel || err) != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ---------------------- Create the input arrays in device memory for our calculation ---------------
	input = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * count, NULL, NULL);						// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateBuffer.html
	if (!input)
	{
		printf("Error: Failed to allocate input device memory!\n");
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}

	// ---------------------- Create the output arrays in device memory for our calculation --------------
	output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * count, NULL, NULL);
	if (!output)
	{
		printf("Error: Failed to allocate output device memory!\n");
		clReleaseMemObject(input);				// Free input buffer on error
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ------------------- Write our data set into the input array in device memory ----------------------
	err = clEnqueueWriteBuffer(commands, input, CL_TRUE, 0, sizeof(float) * count, data, 0, NULL, NULL);		// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clEnqueueWriteBuffer.html
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to write to source array!\n");
		clReleaseMemObject(output);				// Free output buffer on error
		clReleaseMemObject(input);				// Free input buffer on error
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ---------------------------- Set the arguments to our compute kernel ------------------------------
	err = 0;
	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);													// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clSetKernelArg.html
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
	err |= clSetKernelArg(kernel, 2, sizeof(unsigned int), &count);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", err);
		clReleaseMemObject(output);				// Free output buffer on error
		clReleaseMemObject(input);				// Free input buffer on error
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ------------- Get the maximum work group size for executing the kernel on the device --------------
	err = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);	// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clGetKernelWorkGroupInfo.html
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to retrieve kernel work group info! %d\n", err);
		clReleaseMemObject(output);				// Free output buffer on error
		clReleaseMemObject(input);				// Free input buffer on error
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ---------------- Execute the kernel over the entire range of our 1d input data set ----------------
	// ------------------ using the maximum number of work group items for this device -------------------
	global = count;
	err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &global, &local, 0, NULL, NULL);					// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clEnqueueNDRangeKernel.html
	if (err)
	{
		printf("Error: Failed to execute kernel!\n");
		clReleaseMemObject(output);				// Free output buffer on error
		clReleaseMemObject(input);				// Free input buffer on error
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// ---------- Wait for the command commands to get serviced before reading back results --------------
	clFinish(commands);																							// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clFinish.html
	// ---------------------------------------------------------------------------------------------------

	// ------------------ Read back the results from the device to verify the output ---------------------
	err = clEnqueueReadBuffer(commands, output, CL_TRUE, 0, sizeof(float) * count, results, 0, NULL, NULL);		// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clEnqueueReadBuffer.html
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to read output array! %d\n", err);
		clReleaseMemObject(output);				// Free output buffer on error
		clReleaseMemObject(input);				// Free input buffer on error
		clReleaseKernel(kernel);				// Free Kernel on error
		clReleaseProgram(program);				// Free Program on error
		clReleaseCommandQueue(commands);		// Free CommandQueue on error
		clReleaseContext(context);				// Free Context on error
		return (EXIT_FAILURE);
	}
	// ---------------------------------------------------------------------------------------------------

	// --------------------------------------- Validate our results --------------------------------------
	correct = 0;
	for (i = 0; i < count; i++)
	{
		if (results[i] == data[i] * data[i])
			correct++;
	}
	// ---------------------------------------------------------------------------------------------------

	// -------------------------- Print a brief summary detailing the results ----------------------------
	printf("Computed '%d/%d' correct values!\n", correct, count);
	// ---------------------------------------------------------------------------------------------------

	// ------------------------------------- Shutdown and cleanup ----------------------------------------
	clReleaseMemObject(output);																					// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clReleaseMemObject.html
	clReleaseMemObject(input);
	clReleaseKernel(kernel);																					// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clReleaseKernel.html
	clReleaseProgram(program);																					// https://www.khronos.org/registry/OpenCL/sdk/1.1/docs/man/xhtml/clReleaseProgram.html
	clReleaseCommandQueue(commands);																			// https://www.khronos.org/registry/OpenCL/sdk/1.1/docs/man/xhtml/clReleaseCommandQueue.html
	clReleaseContext(context);																					// https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clReleaseContext.html
	// ---------------------------------------------------------------------------------------------------

	return (0);
}
